# Fractal Detection Ros Project

#### Instalar o pacote ros-noetic-usb-cam (para trabalhar com câmeras USB no ROS):

Criar um novo ambiente Conda e ativar o ambiente Conda:

```
conda create -n ros_env python=3.8

conda activate ros_env
```

```
sudo apt-get install ros-noetic-usb-cam
```

Instalar o pacote ros-noetic-image-view (para visualização de imagens no ROS):

```
sudo apt-get install ros-noetic-image-view
```

Instalar python3-empy (necessário pra algumas operações de construção do ROS):

```
sudo apt-get install python3-empy

pip install empy
```

Instalar o catkin_pkg no seu ambiente:

```
pip install catkin_pkg

pip install rospkg
```

1. Criar o Workspace ROS

#### Volte para a raiz do seu workspace ROS e compile novamente:

```
mkdir -p ~/ros_fractal_markers_ws/src && cd ~/ros_fractal_markers_ws/src

git clone https://gitlab.com/alanprodam/fractal_detection_ros.git

cd ~/ros_fractal_markers_ws
```

2. Atualizar o path CMakeLists.txt no seu Pacote ROS

#### Após compilar instalar a biblioteca ArUco

Você precisará informar ao seu pacote ROS onde encontrar esta biblioteca.

Volte para o seu pacote ROS:

```
cd ~/ros_fractal_markers_ws/src/fractal_detection_ros/
```

Edite o CMakeLists.txt, adicione o caminho para a biblioteca ArUco, se você a instalou no sistema, ou especifique diretamente no seu pacote. Por exemplo:

```
cmake_minimum_required(VERSION 3.0.2)
project(fractal_detection)

# Adicionar opções de compilação C++
add_compile_options(-std=c++11)

...

# Incluindo a biblioteca ArUco
set(aruco_DIR "/home/<name_pc>/Documents/fractal_markers_project/aruco-3.1.15/build")
find_package(aruco REQUIRED)

# Configuração do catkin_package
catkin_package(
  CATKIN_DEPENDS cv_bridge image_transport roscpp sensor_msgs std_msgs
  DEPENDS OpenCV aruco
)

# Incluir diretórios de cabeçalhos
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
  ${aruco_INCLUDE_DIRS}
)

# Declarar um executável C++ para o nó ROS
add_executable(sample_detection_node src/sample_detection_node.cpp)

# Adicionar dependências ao executável
add_dependencies(sample_detection_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

# Vincular o executável às bibliotecas
target_link_libraries(sample_detection_node
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  ${aruco_LIBS}
)

# Adicionar o novo executável para o script de geração do marcador fractal
add_executable(generate_fractal_marker src/generate_fractal_marker.cpp)

# Adicionar dependências ao novo executável
add_dependencies(generate_fractal_marker ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

# Vincular o novo executável às bibliotecas
target_link_libraries(generate_fractal_marker
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  ${aruco_LIBS}
)

```

3. Atualizar o CMakeLists.txt no seu Pacote ROS

```
cd ~/ros_fractal_markers_ws

catkin_make
```

4. Calibração de câmera


#### Primeiro gerar as imagens:

```
aruco_calibration live:2 aruco_calibration_board_a4.yml camera_result.yml -size 0.038 -save out_put.yalm

```

#### Segundo referenciar as imagens e rodar a calibração:

```
aruco_calibration_fromimages out_camera_calibration.yml src/fractal_detection/src/camera_infos/image_calibration_1920_1080/ -size 0.038
```

#### Dicas em relação ao resultado RMS:

Em calibração de câmeras, um RMS abaixo de 1 é frequentemente considerado muito bom, enquanto valores acima de 1 podem ser aceitáveis, dependendo da precisão exigida pela sua aplicação específica. O erro de reprojeção (repj error) de 1.98789 também fornece uma indicação de quão precisas são as projeções dos pontos 3D para os pontos 2D na imagem. Assim como o RMS, valores mais baixos são melhores.

5. Rodar algoritmo

```
roscore

roslaunch fractal_detection usb_cam_detector.launch

rosrun fractal_detection fractal_detection_node
```
