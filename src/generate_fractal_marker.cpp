#include <aruco/fractallabelers/fractalmarkerset.h>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

int main() {
    try {
        // Configuração do marcador fractal
        std::vector<std::pair<int, int>> regionsConfig = {{10, 8}, {14, 10}, {6, 0}};

        // Tamanho dos bits (último nível, em pixels)
        int bitSize = -1; // -1 para tamanho normalizado do marcador

        // Criar o conjunto de marcadores fractais
        aruco::FractalMarkerSet fractalMarkerSet;
        fractalMarkerSet.create(regionsConfig, bitSize);

        // Gerar a imagem do marcador
        cv::Mat markerImage = fractalMarkerSet.getFractalMarkerImage(600); // Tamanho da imagem em pixels

        // Salvar a imagem
        cv::imwrite("FRACTAL_3L_6.png", markerImage);

        // Mostrar a imagem
        cv::imshow("FRACTAL_3L_6", markerImage);
        cv::waitKey(0);

    } catch (std::exception& ex) {
        std::cerr << "Exception: " << ex.what() << std::endl;
        return -1;
    }

    return 0;
}
