#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/highgui/highgui.hpp>
#include <aruco/aruco.h>

class FractalDetector {
private:
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    aruco::FractalDetector FDetector; // Instância do detector de fractais

public:
    FractalDetector() : it_(nh_) {
        image_sub_ = it_.subscribe("/usb_cam/image_raw", 1, &FractalDetector::imageCallback, this);
        ROS_INFO("FractalDetector node has started.");

        FDetector.setConfiguration(aruco::FractalMarkerSet::FRACTAL_3L_6);
    }

    void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
        ros::Time start_time = ros::Time::now(); // Início da medição do tempo

        cv_bridge::CvImagePtr cv_ptr;
        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        } catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        detectAndDraw(cv_ptr->image);

        ros::Duration duration = ros::Time::now() - start_time; // Fim da medição do tempo
        double fps = 1.0 / duration.toSec(); // Calcula os FPS

        ROS_INFO("Frame processing time: %f seconds, FPS: %f", duration.toSec(), fps);
    }

    void detectAndDraw(cv::Mat& image) {
        if (FDetector.detect(image)) {
            ROS_INFO("Fractal marker detected!");
            FDetector.drawMarkers(image); // Desenha os marcadores se detectados
        } else {
            ROS_INFO("No fractal marker detected.");
        }

        cv::imshow("Fractal Markers", image);
        cv::waitKey(3); // Ainda necessário para processar eventos do GUI
    }
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "fractal_detector");
    FractalDetector fd;
    ros::spin();
    return 0;
}
