/*
* Este código é uma implementaçãode um nó ROS para detecção de marcadores fractais 
* em imagens, utilizando a biblioteca ArUco. Ele lê as configurações do tópico de imagem,
* tamanho do marcador e tipo de fractal a partir dos parâmetros ROS. 
* 
* O programa se inscreve em um tópico de imagem, processa as imagens recebidas 
* para detectar marcadores fractais, e estima a pose dos marcadores. 
* 
* Se um marcador é detectado e sua pose é estimada com sucesso, 
* as posições X, Y e Z são calculadas e publicadas em um tópico ROS 
* para possível uso por outros nós no sistema. 
*
* Se nenhuma imagem for recebida, 
* o programa informa que está aguardando dados da imagem.
*/

#include <ros/ros.h>
// Inclui bibliotecas ROS e OpenCV
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <aruco/aruco.h>
#include "cvdrawingutils.h"
#include <geometry_msgs/Pose.h>

class FractalDetector {
private:
    // Membros da classe para lidar com nós ROS, imagens e detecção de marcadores
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_;
    ros::Publisher pose_pub_;
    aruco::FractalDetector FDetector;
    aruco::CameraParameters CamParams;
    std::string image_topic_;
    std::string calibration_file_path = "";
    float marker_size_;
    int fractal_type_;
    bool image_received_;
    bool draw_3d_;
    bool debug_;

public:
    // Construtor da classe
    FractalDetector() : it_(nh_),
        image_topic_("/cv_camera/image_raw"),
        calibration_file_path("/home/amr/ros_fractal_markers_ws/src/fractal_detection_ros/camera_infos/out_camera_calibration_1280_720.yaml"),
        marker_size_(0.147),
        fractal_type_(aruco::FractalMarkerSet::FRACTAL_3L_6),
        image_received_(false),
        debug_(false),
        draw_3d_(false) {
            nh_.getParam("image_topic", image_topic_);
            nh_.getParam("camera_calibration_file", calibration_file_path);
            nh_.getParam("marker_size", marker_size_);
            nh_.getParam("fractal_type", fractal_type_);
            nh_.getParam("draw_3d", draw_3d_);
            nh_.getParam("debug", debug_);

            image_sub_ = it_.subscribe(image_topic_, 1, &FractalDetector::imageCallback, this);
            pose_pub_ = nh_.advertise<geometry_msgs::Pose>("fractal_pose", 10);
            ROS_INFO("FractalDetector node has started.");

            FDetector.setConfiguration(fractal_type_);

            if (!calibration_file_path.empty()) {
                CamParams.readFromXMLFile(calibration_file_path);

                if (CamParams.isValid()) {
                    ROS_INFO("Camera parameters loaded successfully from %s", calibration_file_path.c_str());
                    CamParams.resize(cv::Size(1280, 720));
                    FDetector.setParams(CamParams, marker_size_);
                } else {
                    ROS_ERROR("Failed to load camera parameters from %s", calibration_file_path.c_str());
                }

            } else {
                ROS_WARN("Camera calibration file path is not provided. Using default parameters.");
            }
        }

    // Callback para processamento de imagens
    void imageCallback(const sensor_msgs::ImageConstPtr& msg) {
        image_received_ = true;
        ros::Time start_time = ros::Time::now();
        cv_bridge::CvImagePtr cv_ptr;

        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        } catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        // Processa a imagem recebida
        detectAndDraw(cv_ptr->image);
        ros::Duration duration = ros::Time::now() - start_time;
        double fps = 1.0 / duration.toSec();

        ROS_INFO("Frame processing time: %f seconds, FPS: %f", duration.toSec(), fps);
    }

    // Detecta e desenha marcadores na imagem
    void detectAndDraw(cv::Mat& image) {
        if (FDetector.detect(image)) {
            ROS_INFO("Fractal marker detected!");
            FDetector.drawMarkers(image); 

            if (FDetector.poseEstimation()) {
                cv::Mat tvec = FDetector.getTvec();
                double X = tvec.at<double>(0,0);
                double Y = tvec.at<double>(1,0);
                double Z = sqrt(pow(X,2) + pow(Y, 2) + pow(tvec.at<double>(2,0),2));

                if (debug_)
                {
                    ROS_INFO("Distance to fractal X marker: %f meters", X);
                    ROS_INFO("Distance to fractal Y marker: %f meters", Y);
                    ROS_INFO("Distance to fractal Z marker: %f meters", Z);
                }


                // Publica a pose estimada
                geometry_msgs::Pose pose_msg;
                pose_msg.position.x = X;
                pose_msg.position.y = Y;
                pose_msg.position.z = Z;
                pose_pub_.publish(pose_msg);

                if (draw_3d_) {
                    FDetector.draw3d(image); // Desenha em 3D se habilitado
                } else {
                    FDetector.draw2d(image); // Caso contrário, desenha em 2D
                }
            } else {
                FDetector.draw2d(image);
            }
            ROS_INFO("----------------------------------------");
        } else {
            ROS_INFO("No fractal marker detected.");
        }
        //cv::imshow("Fractal Markers", image);
        //cv::waitKey(3);
    }

    // Verifica se a imagem foi recebida
    void checkImageReceived() {
        if (!image_received_) {
            ROS_INFO("Waiting for image data from the topic...");
        }
    }
};

int main(int argc, char** argv) {
    ros::init(argc, argv, "fractal_detector");
    FractalDetector fd;
    
    ros::Rate loop_rate(10);
    while (ros::ok()) {
        fd.checkImageReceived();
        ros::spinOnce();
        loop_rate.sleep();
    }
    
    return 0;
}
